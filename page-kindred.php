<?php
/** 
 * Template Name: Kindred Spirits
 * Description: Page template without sidebar
 */
?>

<?php get_header("kindred"); ?>

<?php 

get_template_part( 'loop-kindredspirits' );  

?>


<?php get_footer(); ?>