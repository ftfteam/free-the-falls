var mod_size = 96;


$(document).ready(function() {

    var menu = $('.navbar');
    var eventObj = $('.navbar');


    eventObj.hover(function(e) {
        e.preventDefault();
        menu.toggleClass('menu-over');
    });

    //  var divs = $('header nav.nav_desktop');
    limit = 250; /* scrolltop value when opacity should be 0 */

    $(window).on('scroll', function() {

        var st = $(this).scrollTop();

        if (st <= limit) {
            menu.removeClass("menu-over-collapse");
        } else {
            menu.addClass("menu-over-collapse");
        }

    })

    $(window).trigger("scroll"); // para executar a funcao 1 vez apos o reaload
});