

//carrousel
$(document).ready(function(){

    var SliderTimer = 5000;
    var myVar = setInterval(function(){ myTimer() }, SliderTimer);

    function myTimer() {
        
        var buttonsnav = $(".buttons-headimage li a.active");
        if(buttonsnav.parent().next().length){
            buttonsnav.parent().next().find('a').trigger('click');
        }else{
            $(".buttons-headimage li:first-child a").trigger('click');
        }
    }


    $(".buttons-headimage li a").click(function(){
        var bt = $(this);
        var target = $(this).attr("data-target");
        if($("#"+target).is(":hidden")){
            $(".buttons-headimage li a").removeClass("active");
            bt.addClass("active");
            $(".headimage").stop().fadeOut(600,
                function(){
                    $("#"+target).stop().fadeIn(600);
                }
            );
        }
        clearTimeout(myVar);
        myVar = setInterval(function(){ myTimer() }, SliderTimer);
        return false;

    });

    

});//enddocumentreayd



//menu
$(document).ready(function(){
    $("header nav").mouseover(function(){
        $("header nav").addClass("over");
    });
    $("header nav").mouseout(function(){
        $("header nav").removeClass("over");
    });

});//enddocumentreayd



//decrease size


$(document).ready(function(){
   
    var divs = $('header nav');
    limit = 250;  /* scrolltop value when opacity should be 0 */  

    $(window).on('scroll', function() {
        
        var st = $(this).scrollTop();

        if (st <= limit) {
            divs.removeClass("collapse");
        }else{
             divs.addClass("collapse");
        }
        
    })

    $(window).trigger("scroll");// para executar a funcao 1 vez apos o reaload
});



//submenu

$(document).ready(function(){

    $(".sub-menu").hide(); 
    
    $("#menu-item-60").mouseout(function(){   
        $(".sub-menu").css({"opacity": "0"});
        $(".sub-menu").delay("slow").hide();
       
    });
    $("#menu-item-60").mouseover(function(){
        $(".sub-menu").show();
        $(".sub-menu").css({"opacity": "1"});
    });
});


//funcao para abrir e fechar o mapa do evento
$(document).ready(function(){

    $(".link_location").click(function(){ 
        $('.map_location').toggleClass("map_show");
        console.log("guenta");
    });

  //  $(".link_location").mouseout(function(){
  //      $('.map_location').removeClass("map_show");;
    //    console.log("saiua");
 //   });
});
