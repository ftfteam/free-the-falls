<?php
/*
 * Template Name: Single Resources
 * Description: Page template without sidebar
 */
?>

<?php get_header("resources"); ?>


<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
	the_post(); 
?>

	<h2 class="subpage-title">
		<?php $cat = get_the_category();	?>
		<?php// guenta($cat[0]) ;?>
		<?php echo $cat[0]->name; ?>
	</h2>
	<h3 class="title"><?php the_title(); ?></h3>
	<p class="date"><?php echo get_the_date(); ?></p>	
	<?php the_content(); ?>


 		<?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?> 

	
	<?php
	} // end while
} // end if
?> 


<?php get_footer(); ?>