<html>

	<head>
		<?php wp_ftf_head("vision"); ?>
	</head>
<body>
 <?php get_template_part('social'); ?>

<header>

    <?php get_template_part( 'menu' ); ?>

	<div class="header-container">
		<div class="header-image" id="">
            <div>
                <?php wp_ftf_logo(); ?>
            </div>
		</div>
        <div class="title-container">
            <div class="page-title">
                <?php the_title( '<h1>', '</h1>' ); ?> 
            </div>
        </div>
	</div>
</header>


<main>

	<div class="container">