<?php
/*
 * Template Name: Events
 * Description: Page template without sidebar
 */
?>

<?php get_header("events"); ?>

 

<?php $page = get_the_title(); //getting page name ?>    

<h2 class="subpage-title"><?php echo $page; ?></h2>

<?php get_template_part( 'loop-events' ); ?>

<div class="bottom_bar">
    <p>
        <a href="<?php echo get_permalink(238); ?>">Past events</a>
    </p>
</div>
<?php get_footer(); ?>