<?php 

$args = array( 
'order'   => 'ASC',
'post_type' => 'taglines',
);
$query = new WP_Query( $args ); 
?>



<div class="header-container" id="content-headimage">

		<?php $counter = 1; ?>
		<?php  while ( $query->have_posts() ) : $query->the_post();  ?>

			<div class="headimage" id="head<?=$counter; ?>" style="background-image:url('<?=the_post_thumbnail_url('full'); ?>');<?= ($counter >1) ? 'display:none' : ''  ?>">
				<div class="container">
					<?php wp_ftf_logo(); ?>
					<h2 class="tagline"><?php the_title(); ?></h2>
				</div>
				<?php $counter = $counter + 1; ?>
			</div>

		
		<?php endwhile; ?>

		<ul class="buttons-headimage">
			<?php for($v=1; $v < $counter; $v++):?>
			<li><a data-target="head<?=$v?>" class="<?=($v > 1)?'active':'' ?>"><span>$v</span></a></li>
			<?php endfor; ?>				
		</ul>
	</div>   