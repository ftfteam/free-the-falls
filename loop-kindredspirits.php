<?php

$args = array( 
'order'   => 'ASC',
'posts_per_page' => -1, 
'post_type' => 'kslinks',
);

$query = new WP_Query( $args ); 

function the_url($postid){
    echo get_post_meta($postid,'url')[0];
}
?>
<style>

</style>
<?php if($query->have_posts()): ?>
    <ul class="column-list">
    <?php  while ( $query->have_posts() ) : $query->the_post();  ?>
        <li>
            <?php the_title(); ?>
            <a href="<?php the_url($post->ID);  ?>"><?php the_url($post->ID);  ?></a>
        </li>
    <?php endwhile; ?>
    </ul>
<?php else: ?>
    <p>There are no upcomming events.</p> 
<?php endif; ?> 
