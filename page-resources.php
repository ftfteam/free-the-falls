<?php
/*
 * Template Name: Resources > List
 * Description: Page template without sidebar
 */
?>

<?php get_header("resources"); ?>

 

<?php $page = get_the_title(); //getting page name ?>    

<h2 class="subpage-title"><?php echo $page; ?></h2>

<?php


get_template_part( 'loop-resources' );  
?>


<?php get_footer(); ?>