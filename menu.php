
<nav class="nav_desktop">
		<div class="container">

				<img class="logo" src="<?php echo ftf_logotype(); ?>" />

			<?php wp_nav_menu( array( 
				'menu' => 'ftf', 
			) ); ?>

			
			

		</div>


</nav>

<nav class="nav_mobile">
		<div class="logo_container">
			<img class="logo" src="<?php echo ftf_logotype(); ?>" />
		</div>	
		<div class="container">

			<?php wp_nav_menu( array( 
				'menu' => 'ftf_mobile', 
			) ); ?>
		</div>
</nav> 