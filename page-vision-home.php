<?php
/*
 * Template Name: Vision > Front Page
 * Description: Page template without sidebar
 */
?>

<?php get_header("thevision"); ?>



<?php $args = array(
	'sort_order' => 'asc',
	'sort_column' => 'menu_order',
	'hierarchical' => 1,
	'exclude' => '',
	'include' => '',
	'meta_key' => '',
	'meta_value' => '',
	'authors' => '',
	'child_of' => 49,
	'parent' => -1,
	'exclude_tree' => '',
	'number' => '',
	'offset' => 0,
	'post_type' => 'page',
	'post_status' => 'publish'
); 
$pages = get_pages($args); 

//guenta($pages);

?>



<?php foreach ($pages as $page){ ?>

    <div>
        <h3><a href="<?php echo $page->guid; ?>"><?php echo $page->post_title;?></a></h3>
        <?php ftf_show_thumbnail($page->ID); ?>
        <p>
			<?php echo $page->post_excerpt; ?><br/>
			<a href="<?php echo $page->guid; ?>">Read more</a>
		</p>
        
    </div>


<?php } ?>


<?php get_footer(); ?>