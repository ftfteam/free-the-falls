<?php
//http://www.wpbeginner.com/wp-tutorials/25-extremely-useful-tricks-for-the-wordpress-functions-file/


/**********************************************************************************
wpmu_theme_support - adds theme support for post formats, post thumbnails, HTML5 and automatic feed links
**********************************************************************************/



header("Access-Control-Allow-Origin: *");

add_theme_support( 'post-thumbnails' );
add_action( 'init', 'my_add_excerpts_to_pages' );

function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}



 

function wp_ftf_logo(){
	$url = get_template_directory_uri();
    echo '<img class="main_logo" src="'.$url.'/styles/freethefalls_logo.svg" />'; 
}

function ftf_logotype(){
	$url = get_template_directory_uri();
	$nurl = $url.'/styles/freethefalls_logotype.svg';
	return $nurl;
}
 

function wp_ftf_head($css){
$url = get_template_directory_uri();
    echo '
<link href="https://fonts.googleapis.com/css?family=Marmelad" rel="stylesheet">
    	<meta charset="UTF-8">
	<link rel="stylesheet" href="'.$url.'/styles/'.$css.'.css" /> 
    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script src="'.$url.'/common/scripts.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    ';
} 


//function to charge params to a wordpress query
function ftf_chargeParams($post_type){

	$args = array( 
	'order'   => 'ASC',
	'posts_per_page' => -1, 
	'post_type' => $post_type,
	'category_name' => 'staff'
	 );
	$query = new WP_Query( $args ); 

	return $query;

}


//Load a specific template part based on the current pagename:

function ftf_templatePageName($prefix,$pageName){
	if (locate_template('content-' . $pageName . '.php') != '') {
		// yep, load the page template
		get_template_part('content', $pageName);
	} else {
		// nope, load the content
		the_content();
	}
}

//break an URL and return slug 

function ftf_getSlugFromUrl($str){
	$rest = explode('/',$str);
	return $rest[5];
} 



//debug_backtrace
function guenta($param){
    echo "<pre style='width:300px'>";
    print_r($param);
    echo "</pre>";
}


function ftf_show_thumbnail($post_id){
    $thumb = get_the_post_thumbnail( $post_id, 'thumbnail' );  
    echo $thumb;
}




/*

POST TYPES

*/


function ftf_resources(){

	$labels = array(
		'name'               => _x( 'Resources', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Resource', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Resources', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Resource', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'resource', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New resource', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New resource', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit resource', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View resource', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All resources', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search resources', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent resources:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No resources found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No resources found in Trash.', 'your-plugin-textdomain' )
	);

	$rewrite = array(
		'slug'                       => 'resource',
		'with_front'                 => true,
		'hierarchical'               => true,
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => true,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','comments' )
	);

   // $categories = array('news','multimedia','research documents','handouts');
 //   register_taxonomy( 'type',$categories, $args ); 
    
	register_post_type( 'resource', $args );
    register_taxonomy_for_object_type( 'category', 'resource' );

}//end ftf_resources

add_action('init', 'ftf_resources');
//call action to init ftf_resources function to create a new posttype




function ftf_events(){

	$labels = array(
		'name'               => _x( 'Events', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Event', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Events', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'event', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New event', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New event', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit event', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View event', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All events', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search events', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent events:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No events found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No events found in Trash.', 'your-plugin-textdomain' )
	);

	$rewrite = array( 
		'slug'                       => 'events',
		'with_front'                 => true,
		'hierarchical'               => true,
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => true,
		'supports'           => array( 'title', 'editor', 'thumbnail','custom-fields' )
	);


    
	register_post_type( 'event', $args );

}//end ftf_events

add_action('init', 'ftf_events');


function truncateText($string, $limit, $break=".", $pad="...")
{
  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit) return $string;

  // is $break present between $limit and the end of the string?
  if(false !== ($breakpoint = strpos($string, $break, $limit))) {
    if($breakpoint < strlen($string) - 1) {
      $string = substr($string, 0, $breakpoint) . $pad;
    }
  }

  return $string;
}



class Location{


    public function __construct($post){

		$this->name_location = get_post_meta($post,'name_of_location')[0]; 
		$this->date = get_post_meta($post,'date')[0];
		$this->from_time = get_post_meta($post,'from_time')[0];
		$this->to_time = get_post_meta($post,'to_time')[0];
		$this->location = get_post_meta($post,'location')[0];   

    }

	public function loc_name(){
		echo $this->name_location;
	}

	public function loc_date(){
		echo $this->date;
	}

	public function loc_date_day(){
		$d =  explode('-',$this->date);
		echo $d[2]; 
	}

	public function loc_date_month(){
		$d =  explode('-',$this->date);
		$monthNum = $d[1];
		$monthName = date("M", mktime(0, 0, 0, $monthNum, 10));
		echo $monthName; 
	}

	public function loc_date_year(){
		$d =  explode('-',$this->date);
		echo $d[0]; 
	}	

	public function loc_from(){
		echo $this->from_time;
	}

	public function loc_to(){
		echo $this->to_time;
	}

	public function loc_location(){
		echo $this->location;
	}
	public function get_loc_location(){
		return $this->location;
	}					

}// end of location



?>