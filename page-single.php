<?php
/** 
 * Template Name: Single Page
 * Description: 
 */
?>

<?php get_header("vision-detail-nophoto"); ?>

<?php 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} 
}

?>


<?php get_footer(); ?>