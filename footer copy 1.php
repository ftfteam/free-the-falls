	</div>
</main>
<footer>
	<div class="container">
	<img class="logo" src="<?php bloginfo('template_directory'); ?>/styles/freethefalls_logotype.svg" />
		<?php wp_nav_menu( array( 'ftf', 'depth'=> 1 , 'container_class'=>'footer_menu') ); ?>
		<span class="copyright">© FREE THE FALLS</span>
	</div>
</footer>
</body>