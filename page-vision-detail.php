<?php
/*
 * Template Name: Vision > Detail > Show Photo
 * Description: Page template without sidebar
 */
 
?>

<?php get_header("vision-detail"); ?>

<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>

		
		<div class="img">
			<?php the_post_thumbnail('full', array( 'class' => 'image' )); ?>
		</div>
		<?php the_content(); ?>

	<?php } // end while ?>
<?php } // end if ?>


<?php get_footer(); ?>