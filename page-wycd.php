<?php
/*
 * Template Name: What you can do
 * Description: 
 */
?>

<?php get_header("wycd"); ?>


<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
 
		the_content();
		//
		// Post Content here
		//
	} // end while
} // end if
?>


<?php get_footer(); ?>