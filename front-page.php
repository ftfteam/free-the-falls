<?
/*
https://codex.wordpress.org/The_Loop
https://codex.wordpress.org/Template_Tags
https://www.siteground.com/tutorials/wordpress/wordpress_create_theme.htm

<?php bloginfo('template_directory'); ?>
*/?>
<?php get_header("home"); ?>
<section id="blog">
    
<?php get_template_part( 'front-page-news-list' ); ?>
</section>
<aside id="sidebar">
    <?php get_template_part( 'front-page-event' ); ?>           
 <?php get_template_part( 'front-page-sign' ); ?>    
</aside> 


<?php get_footer(); ?>