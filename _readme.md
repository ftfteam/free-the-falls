# Wordpress Snippets

### Creating Post Type Session

1. Add a new post type using WP Post Type Plugin 
2. Customize information
3. Create Loops


#### Including Loop
```php
get_template_part( 'loop-resources' );  
```

#### Showing page content
```php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} 
}
```

#### Looping in an easy mode
```php
<?php

$args = array( 
'order'   => 'DESC',
'posts_per_page' => -1, 
'post_type' => 'resource',
'category_name' => $cat
);

$query = new WP_Query( $args ); 

?>


<?php if($query->have_posts()): ?>
    <?php  while ( $query->have_posts() ) : $query->the_post();  ?>
 

    <?php endwhile; ?>
<?php else: ?>
    <p>There are no upcomming events.</p> 
<?php endif; ?>
```

#### Looping using date as Custom Field
```php
<?php 
$cat = $post->post_name;
$date_now = date('Y-m-d H:i:s');
$args = array( 
'order'   => 'DESC',
'posts_per_page' => -1, 
'post_type' => 'event',
'orderby'			=> 'meta_value',
'meta_type'			=> 'DATETIME',
'meta_query'	=> array(
		'relation'		=> 'AND',
	    array(
	        'key'			=> 'date',
	        'compare'		=> '>=',
	        'value'			=> $date_now,
	        'type'			=> 'DATETIME'
	    )
	)
);
$query = new WP_Query( $args ); 

?>


<?php if($query->have_posts()): ?>
    <?php  while ( $query->have_posts() ) : $query->the_post();  ?>


    <?php endwhile; ?>
<?php else: ?>
    <p>There are no upcomming events.</p> 
<?php endif; ?>

``` 
