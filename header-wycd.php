<html>

	<head>
		<?php wp_ftf_head("wycd"); ?>
	</head>
<body>
<?php get_template_part('social'); ?>

<header>

    <?php get_template_part( 'menu' ); ?>

	<div class="header-container">
		<div class="header-image" id="">
            <div>
                <?php wp_ftf_logo(); ?>
            </div>
		</div>
        <div class="title-container">
            <div class="page-title">
                <?php the_title( '<h1>', '</h1>' ); ?>
            </div>
        </div>
	</div>
</header>


<main>
    <div class="tagline-container">
        <h3 class="tagline">Please donate to support our vision. Your contribution will help us defray the cost of communications products, travel, and legal challenges.
        </h3>
    </div>
	<div class="container">