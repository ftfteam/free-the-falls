<?php
/*
 * Template Name: Single Events
 * Description: Page template without sidebar
 */
?>

<?php get_header("events"); ?>
<script>
$(document).ready(function(){

	var scroll_limit = 70;
	var element = $('.timeevent');
	var window = $(window);
	
	if(window.width() < 320){
		window.scroll(function(){
			var current_scroll = $(window).scrollTop();

			if(current_scroll > scroll_limit){
				element.css({"position": "fixed", "top": "96px"});		
			//	console.log("aaa");
			}else{
				element.css({"position": "static", "top": "0"});
			//	console.log("111");
			}
		});
	};  
});

</script>
<div class="event_detail">
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
	the_post(); 
?>
<?php $l = new Location($post->ID); ?>

	<div class="timeevent">
		<span class="month"><?php $l->loc_date_month(); ?></span>
		<span class="day"><?php $l->loc_date_day(); ?></span>
		<span class="hour"> <?php $l->loc_from(); ?> - <?php $l->loc_to(); ?> </span>
	</div>

	<div class="content">
		<h3 class="title"><?php the_title(); ?></h3>
			
		<?php if($l->get_loc_location() != ''): ?>	
			<p class="location"><a class="link_location"><?php $l->loc_name(); ?></a></p>
		<?php else: ?>
			<p class="location"><?php $l->loc_name(); ?></p>
		<?php endif; ?>
		<?php if($l->get_loc_location() != ''): ?>	
			<div class="map_location"><?php $l->loc_location(); ?></div>
		<?php endif; ?>
		<?php the_content(); ?>
	</div>
 	<?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?> 
	<?php
	} // end while
} // end if
?> 
</div> 

<?php get_footer(); ?>