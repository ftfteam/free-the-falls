<?php 


$cat = $post->post_name;

$args = array( 
'order'   => 'DESC',
'posts_per_page' => -1, 
'post_type' => 'resource',
'category_name' => $cat
);

$query = new WP_Query( $args ); 

?>
<?php if($query->have_posts()): ?>

    <?php  while ( $query->have_posts() ) : $query->the_post();  ?>

        

        <div class="list"> 

            <?php  ftf_show_thumbnail($post->ID) ?>
        
            <p class="date"><?php the_time('F jS, Y') ?></p>
            <a href="<?php the_permalink(); ?>"><h3 class="title"><?php echo $post->post_title;?></h3></a>       
            <p class="text"><?php the_excerpt(); ?></p>     
        </div>


    <?php endwhile; ?>
<?php else: ?>
    <p>There are no posts in this section.</p> 
<?php endif; ?>