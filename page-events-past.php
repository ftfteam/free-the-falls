<?php
/*
 * Template Name: Events > Past Events
 * Description: Page template without sidebar
 */
?>

<?php get_header("events"); ?>

 

<?php $page = get_the_title(); //getting page name ?>    

<h2 class="subpage-title"><?php echo $page; ?></h2>
<div class="events_past">
    <?php get_template_part( 'loop-events-past' ); ?>
</div>

<?php get_footer(); ?>