<html>

	<head>
		<?php wp_ftf_head("resources"); ?>
	</head>
<body>
<?php get_template_part('social'); ?>

<header>

    <?php get_template_part( 'menu' ); ?>

	<div class="header-container">
		<div class="header-image" id="">
            <div>
                <?php wp_ftf_logo(); ?>
            </div>
		</div>
        <div class="title-container">
            <div class="page-title">
                <h1>Resources</h1>
            </div>
        </div>
	</div>
</header>


<main>

    <div class="submenu">
			<div class="menu-resources-container">
                <?php 
                    $menu = wp_get_nav_menu_items( "resources" ); 
                    
                    //echo $ftf_slug;
                ?>    

                <ul>
                    <?php foreach($menu as $item): ?>
                        <?php $ftf_slug = ftf_getSlugFromUrl($item->url); ?>
                        <li>
                           <?php if($post->post_name == $ftf_slug): ?>
                            <a class="active" href="<?php echo $item->url; ?>">
                            <?php else: ?>
                            <a href="<?php echo $item->url; ?>">
                            <?php endif; ?>
                                <?php echo $item->title; ?>
                                <span class="count">
                        
                                    <?php 
                                        $category = get_category_by_slug($ftf_slug);
                                        //guenta($category);
                                        $count = $category->category_count;
                                        echo $count;
                                    ?>
                                </span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>                 
            </div><!-- end menu-resources-container -->
    </div>

	<div class="container">
