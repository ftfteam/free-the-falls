<?php
$args = array( 
'order'   => 'DESC', 
'posts_per_page' => 3, 
'post_type' => 'resource',
'category_name' => 'news'
);

$query = new WP_Query( $args ); 
?>
    
<!-- BLOG Section -->
<?php  while ( $query->have_posts() ) : $query->the_post();  ?>
<!-- Start Loop -->


<article class="entry">
	<?php the_post_thumbnail('thumbnail', array( 'class' => 'image' )); ?>
	<h3 class="title">
		<a href="<?php the_permalink(); ?>">
			<?php the_title(); ?>
		</a>
	</h3>
	<p class="date"><?php the_time('F jS, Y') ?></p>
	<p class="text"><?php the_excerpt(); ?> <!--a href="<?php the_permalink(); ?>">Read more</a--></p>	
	<span class="comments">
		<p>Comments <span class="number">
					
		 			<?php comments_number( '0', '1', '%' ); ?>
				
		</span></p>
	</span>
</article> 


<!-- end of loop -->
<?php  endwhile; ?>
<!-- end of Blog section -->