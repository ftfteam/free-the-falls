<?php
/*
 * Template Name: Vision > Detail > No Photo
 * Description: 
 */
 
?>

<?php get_header("vision-detail-nophoto"); ?>

<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>

		<?php the_content(); ?>

	<?php } // end while ?>
<?php } // end if ?>


<?php get_footer(); ?>