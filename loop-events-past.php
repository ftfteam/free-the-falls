
<?php 


$cat = $post->post_name;
$date_now = date('Y-m-d H:i:s');
$args = array( 
'order'   => 'DESC',
'posts_per_page' => -1, 
'post_type' => 'event',
'orderby'			=> 'meta_value',
'meta_type'			=> 'DATETIME',
// 'meta_key'=> 'past_event',
// 'meta_value'	=> 1
'meta_query'	=> array(
		'relation'		=> 'AND',
	    array(
	        'key'			=> 'date',
	        'compare'		=> '<',
	        'value'			=> $date_now,
	        'type'			=> 'DATETIME'
	    )
	)
);

$query = new WP_Query( $args ); 

?>
 <div class="list">
  <?php if($query->have_posts()): ?>
<?php  while ( $query->have_posts() ) : $query->the_post();  ?>
        <?php $l = new Location($post->ID); ?>
 
        <div class="event_item">          
            <div class="container">
                <div class="timeevent">
                    <span class="month"><?php $l->loc_date_month(); ?></span>
                    <span class="day"><?php $l->loc_date_day(); ?></span>
                    <span class="hour"> <?php $l->loc_from(); ?> - <?php $l->loc_to(); ?> </span>
                </div>
                <div class="information">
                    <a href="<?php the_permalink(); ?>"><span class="eventname"><?php the_title(); ?></span></a>
                    <span class="place">
                    <img class='ico_pin' src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/pin.svg" />
                    <span><?php $l->loc_name(); ?></span>
                    </span>
                    
                    <span class="moreinfotitle">MORE INFO</span>
                    <p class="moreinfo"><?php echo truncateText(get_the_excerpt(),100); ?></p>
                                
                </div>				   
            </div>  
        </div>

<?php endwhile; ?>

<?php else: ?>
    <p>There are no past events.</p> 
<?php endif; ?>
</div>